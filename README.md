# RPG Characters

Console Application developed with C#

<h3>Requirements for the App</h3>
<hr>

<ul>
<li>Various character classes having attributes which increase at different rates as the character gains
levels</li>
<li>Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the
power of the character, causing it to deal more damage and be able to survive longer. Certain
characters can equip certain item types.</li>
<li>Summary (///) tags for each method you write, explaining what the method does, any exceptions it
can throw, and what data it returns (if applicable). You do not need to write summary tags for
overloaded methods.</li>
<li>Full test coverage of the functionality</li>
</ul>

<h4>In the game there are currently four classes that a character/hero can be</h4>
<ul>
<li>Mage</li>
<li>Ranger</li>
<li>Rogue</li>
<li>Warrior</li>
</ul>

<h4>Characters in the game have several types of attributes, which represent different aspects of the character. They are
divided into two groups:</h4>
<ul>
<li>Primary attributes</li>
<li>Secondary attributes</li>
</ul>
